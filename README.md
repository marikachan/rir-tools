# rir-tools

Assemble tools for pear-9 and other my homebrew machines

## Introduction

This contains several tools useful to assemble program image runs
[pear-9](https://codeberg.org/marikachan/pear-emulator) and other my homebrew 
machines.

## How it work

Since instruction of pear-9 and unit of RAM is 12-bit (more details for 
"Why there's no raw binary?", thus I needed to put specific binary 
format for bootable pear-9 image and I named rir(shotage of 'R'AM Image 
aRchive).

To assemble .rir file, run asmrir. Consist file formats is berow.

## Formats

WIP

| File Extension | Description |
|:--:|:--|
|.iys|Initial Memory State file(root of assembly)|
|.dys|DataSet description file(ex.DOS BPB)|
|.ayf|Assembly Function code file(well known as assambly code)|

## Why there's no raw binary?

WIP


